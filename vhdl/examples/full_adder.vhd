use ternary.all;

entity main is
    Port ( a : in FakeTrit;
           b : in FakeTrit;
           c : in FakeTrit;
           s : out FakeTrit;
           c2 : out FakeTrit
          );
end main;

architecture Behavioral of main is
signal r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14 : FakeTrit;
begin
process (a,b,c)
begin
   r1 <= mux(a,O,P,N);
   r2 <= mux(a,P,N,O);
   r3 <= mux(b,r1,r2,a);
   r4 <= mux(b,r2,a,r1);
   r5 <= mux(b,a,r1,r2);
   r6 <= mux(c,r3,r4,r5);
   r7 <= e21(a,N,O);
   r8 <= e12(a,N,O);
   r9 <= e21(a,O,P);
   r10 <= e12(a,O,P);
   r11 <= mux(b,r7,r8,O);
   r12 <= mux(b,r8,O,r9);
   r13 <= mux(b,O,r9,r10);
   r14 <= mux(c,r11,r12,r13);
   s <= r6;
   c2 <= r14;
end process;
end Behavioral;
