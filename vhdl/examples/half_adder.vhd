use ternary.all;

entity main is
    Port ( a : in FakeTrit;
           b : in FakeTrit;
           c : out FakeTrit;
           s : out FakeTrit );
end main;

architecture Behavioral of main is
signal r1,r2,r3,r4,r5,r6 : FakeTrit;
begin
process (a,b)
begin
    r1 <= mux(a,P,N,O);
    r2 <= mux(a,O,P,N);
    r3 <= mux(b,r1,a,r2);
    r4 <= e12(a,N,O);
    r5 <= e21(a,O,P);
    r6 <= mux(b,r4,O,r5);
    s <= r3;
    c <= r6;
end process;
end Behavioral;
